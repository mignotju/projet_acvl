#Maekfile projet ACVL

all: clean compile simu

simu:
	java -classpath build SimuBourse

compile:
	javac -d build -classpath build -sourcepath src src/SimuBourse.java

clean:
	rm -rf build/*
