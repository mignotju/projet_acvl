
import gui.*;
import modele.*;
import controleur.*;

public class SimuBourse {

    public static void main(String[] args) {
        Cours cours = new Cours();
        cours.miseAJourBourse();
        MainFrame w = new MainFrame(1200, 720, Utilisateur.INSTANCE, cours);
        Controleur.BourseMAJTimer(5000, cours);
    }
}
