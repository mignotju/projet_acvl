/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controleur;

import java.util.*;
import modele.ActionAchatPrecis;
import modele.ActionPf;
import modele.ActionVentePrecis;
import modele.Cours;
import modele.Utilisateur;

public class Controleur {

    public static void BourseMAJTimer(int timer, Cours c) {
        while (true) {
            try {
                Thread.sleep(timer);
            } catch (Exception e) {
            }
            c.miseAJourBourse();
            Utilisateur.INSTANCE.miseAJourAttentes(c);
        }
    }

    //Je récupere le nombre d'ancienne action en attente.
    //Je crée un nouvel objet attente qui a pour quantite la nouvelle plus l ancienne.
    //J'écrase l'ancienne et remplace par la nouvelle.
    //Ensuite, il faut que à chaque maj, on test toute la map Buyattente, et SellAttente.
    //Si prix ok, on buy ou sell.
    //Je fais ces fonctions dans ActionAchatPrecis et ActionVentePrecis
    public static void addBuyAttente(String name, int q, double prixAttendu) {
        int oldQ = 0;
        Date d = new Date();
        if (Utilisateur.INSTANCE.getBuyAttentes().containsKey(name)) {
            oldQ = Utilisateur.INSTANCE.getBuyAttentes().get(name).getQuantite();
        }
        ActionAchatPrecis actionToAdd = new ActionAchatPrecis(d.getTime(), name, q + oldQ, prixAttendu);
        Utilisateur.INSTANCE.getBuyAttentes().put(name, actionToAdd);
        Utilisateur.INSTANCE.informe();
    }

    public static void addSellAttente(String name, int q, double prixAttendu) {
        int oldQ = 0;
        Date d = new Date();
        if (Utilisateur.INSTANCE.getSellAttentes().containsKey(name)) {
            oldQ = Utilisateur.INSTANCE.getSellAttentes().get(name).getQuantite();
        }
        ActionVentePrecis actionToAdd = new ActionVentePrecis(d.getTime(), name, q + oldQ, prixAttendu);
        Utilisateur.INSTANCE.getSellAttentes().put(name, actionToAdd);
        Utilisateur.INSTANCE.informe();
    }

    public static void buy(String name, int q, double prix) {
        double oldPrice = 0;
        int oldQuantite = 0;
        double oldPrixActuel = prix;
        Map<String, ActionPf> pf = Utilisateur.INSTANCE.getPF();
        if (Utilisateur.INSTANCE.getCash() >= q * prix) {
            if (pf.containsKey(name)) { //J'ai déjà dans mon pf cette action --> a
                ActionPf a = pf.get(name);
                oldPrice = a.getBuyPrice();
                oldQuantite = a.getQuantite();
                oldPrixActuel = a.getPrixActuel();
                pf.remove(name); // je l'enleve, et le remet après
            }
            ActionPf actionToAdd = new ActionPf(name, q + oldQuantite, (prix * q + oldPrice * oldQuantite) / (q + oldQuantite), oldPrixActuel);
            pf.put(name, actionToAdd);
            Utilisateur.INSTANCE.setCash(Utilisateur.INSTANCE.getCash() - q * prix);
        }
    }

    public static void sell(String name, int q, double prix) {
        double oldPrice = 0;
        int oldQuantite = 0;
        double oldPrixActuel;
        Map<String, ActionPf> pf = Utilisateur.INSTANCE.getPF();
        if (!pf.containsKey(name)) {
            return; //pas de vente
        }
        ActionPf actionToSell = pf.get(name);
        if (actionToSell.getQuantite() < q) {
            int oldQ = q;
            q = actionToSell.getQuantite();
        }

        Utilisateur.INSTANCE.setCash(Utilisateur.INSTANCE.getCash() + q * prix);
        oldQuantite = actionToSell.getQuantite();
        oldPrice = actionToSell.getBuyPrice();
        oldPrixActuel = actionToSell.getPrixActuel();
        pf.remove(name);
        if (oldQuantite != q) {
            ActionPf actionToAdd = new ActionPf(name, oldQuantite - q, oldPrice, oldPrixActuel);
            pf.put(name, actionToAdd);
        }
    }
}
