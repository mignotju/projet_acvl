/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JDialog;
import javax.swing.JTextField;


public abstract class ActionDialog extends JDialog{
    
    private ButtonOk okBt;
    private JTextField qtyField;
    private JTextField priceField;
    
    public ActionDialog(int x, int y, modele.Action a,ActionPanel pnl) {
        setSize(x, y);
        setLocationRelativeTo(null);
        pnl.setLayout(null);
        /*Ajout du champs de saisie de la quantité*/
        qtyField = new JTextField();
        qtyField.setBounds(50, 30, 70, 20);
        qtyField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    pnl.setCout(pnl.getPrixAttendu() * Integer.parseInt(qtyField.getText()));
                    pnl.repaint();
                } catch (NumberFormatException ex) {
                    
                   
                }
            }
        });
        pnl.add(qtyField);
        /*Ajout du champs de saisie du prix voulu si besoin*/
        priceField = new JTextField();
        priceField.setBounds(220, 30, 70, 20);
        priceField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    pnl.setPrixAttendu(Double.parseDouble(priceField.getText()));
                    pnl.setCout(pnl.getPrixAttendu() * Integer.parseInt(qtyField.getText()));
                    pnl.repaint();
                } catch (NumberFormatException ex) {
                    
                   
                }
            }
        });
        pnl.add(priceField);
        /*Ajout du bouton de fermeture*/
        okBt = new ButtonOk(this);
        okBt.setBounds(getWidth()/2-30, 145, 60, 20);
        pnl.add(okBt);
        setContentPane(pnl);
        setVisible(true);
    }
    
    public ButtonOk getBtOk() {
        return okBt;
    }
    
    public String getQtyField() {
        return qtyField.getText();
    }
    
    public String getPriceField(){
        return priceField.getText();
    }
}
