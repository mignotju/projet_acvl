/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import javax.swing.JPanel;


public abstract class ActionPanel extends JPanel{
    private modele.Action action;
    private double cout;
    private double prixAttendu;

    public ActionPanel(modele.Action a) {
        this.action = a;
        cout=0;
        prixAttendu=a.getPrixActuel();
    }
    
    public double getCout() {
        return cout;
    }
    
    public modele.Action getActif() {
        return action;
    }
    
    public void setCout(double c) {
        cout = c;
    }
    public void setPrixAttendu(double p){
        prixAttendu=p;
    }
    
    public double getPrixAttendu(){
        return prixAttendu;
    }
}
