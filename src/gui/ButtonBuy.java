package gui;
import modele.Action;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonBuy extends ButtonAction{

	public ButtonBuy(Action a) {
		super("BUY");
		addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				BuyDialog buyFrame = new BuyDialog(320,200,a);
			}
		});
	}
	
}
