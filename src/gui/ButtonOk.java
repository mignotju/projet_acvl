/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;


public class ButtonOk extends JButton{
    
    public ButtonOk(ActionDialog d) {
        super("Ok");
        addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent e) {
                            //cette action se fera après l'action du buy/sell
				d.dispose();
			}
		});
    }
}
