package gui;
import modele.Action;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ButtonSell extends ButtonAction{

	public ButtonSell(Action a) {
		super("SELL");
		addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				SellDialog sellFrame = new SellDialog(320,200,a);
			}
		});
	}
	
}