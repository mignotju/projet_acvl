package gui;

import java.awt.event.*;
import controleur.*;

public class BuyDialog extends ActionDialog {

    public BuyDialog(int x, int y, modele.Action a) {
        super(x, y, a, new BuyPanel(a));
        setTitle("BUY " + a.getName());
        /*On achete l'action  la fermeture de la fenetre*/
        getBtOk().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                /*On buy nos actions*/
                try {
                    int quantite = Integer.parseInt(getQtyField());
                    double prix = a.getPrixActuel();
                    if (quantite != 0) {
                        if (!getPriceField().isEmpty()) {
                            prix = Double.parseDouble(getPriceField());
                        }
                        if (prix == a.getPrixActuel()) {
                            /*Achat direct*/
                            Controleur.buy(a.getName(), quantite, a.getPrixActuel());
                        } else {
                            /*Achat en attente*/
                            Controleur.addBuyAttente(a.getName(), quantite, prix);
                        }
                    }
                } catch (NumberFormatException ex) {

                }
            }
        });
    }
}
