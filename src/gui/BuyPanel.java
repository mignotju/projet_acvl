package gui;

import java.awt.Graphics;
import javax.swing.*;
import modele.*;

public class BuyPanel extends ActionPanel {

    public BuyPanel(modele.Action a) {
        super(a);
    }

    @Override
    protected void paintComponent(Graphics g) {
        g.clearRect(0, 0, getWidth(), getHeight());
        g.drawString("Achat de l'action " + getActif().getName() + " au prix de " + getActif().getPrixActuel() + "€/µ", 5, 20);
        g.drawString("Qty : ", 20, 45);
        g.drawString("Prix précis : ", 150, 45);
        g.drawString("(pressez 'entrée' pour obtenir le cout)", 20, 60);
        g.drawString("Cout de l'opération -" + getCout() + "€", 20, 85);
        if (getPrixAttendu() != getActif().getPrixActuel()) {
            g.drawString("Achat si le cours de l'actif atteint " + getPrixAttendu() + "€", 20, 105);
        } else {
            g.drawString("Achat direct", 20, 105);
        }
    }
}
