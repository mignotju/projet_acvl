package gui;

import javax.swing.*;
import modele.*;

public class MainFrame extends JFrame {

    public MainFrame(int x, int y, Utilisateur user, Cours cours) {
        setTitle("Jeu de simulation Boursière");
        setSize(x, y);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        MainPanel fond = new MainPanel(user, cours);
        user.attache(fond);
        cours.attache(fond);
        fond.setLayout(null);
        setContentPane(fond);
        setVisible(true);
    }
}
