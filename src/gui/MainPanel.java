package gui;

import java.awt.Color;
import java.awt.Font;
import modele.*;
import java.awt.Graphics;
import java.util.*;
import javax.swing.BorderFactory;

import javax.swing.JPanel;

public class MainPanel extends JPanel implements Observateur {

    private Cours cours;

    public MainPanel(Utilisateur u, Cours crs) {
        cours = crs;
    }

    public Cours getCours() {
        return cours;
    }

    @Override
    protected void paintComponent(Graphics g) {
        int y = 30;
        g.clearRect(0, 0, getWidth(), getHeight());
        removeAll();
        g.setFont(new Font("default", Font.BOLD, 14));
        g.drawRoundRect(0, 0, getWidth() / 2, getHeight(), 5, 5);
        g.drawRoundRect(getWidth() / 2, 0, getWidth(), getHeight(), 5, 5);
        g.drawString("Cash:  " + Utilisateur.INSTANCE.getCash(), getWidth() / 2 + 2, getHeight() - 32);

        /*Affiche des cours*/
        g.drawString("Cours", 2, 12);
        g.drawString("Société", 2, y);
        g.drawString("Prix", 122, y);
        g.drawString("Prix Open", 242, y);
        g.drawString("Value", 362, y);
        g.setFont(new Font("default", Font.ROMAN_BASELINE, 14));
        y += 15;
        Iterator<String> itCours = cours.getIterator();
        while (itCours.hasNext()) {
            ActionCours a = cours.getListe().get(itCours.next());
            ButtonBuy bt = new ButtonBuy(a);
            add(bt);
            bt.setBounds(440, y - 14, 50, 14);
            bt.setBorder(BorderFactory.createEmptyBorder());
            g.drawString(a.getName(), 2, y);
            g.drawString(Double.toString(a.getPrixActuel()) + "€", 122, y);
            g.drawString(Double.toString(a.getPrixOpen()) + "€", 242, y);
            colorString(g, a.getValueOpen(), 362, y);
            y += 15;
        }
        y = 30;
        /*Affichage du PTF*/
        g.setFont(new Font("default", Font.BOLD, 14));
        g.drawString("Portefeuille", getWidth() / 2 + 2, 12);
        g.drawString("Société", getWidth() / 2 + 2, y);
        g.drawString("Quantité", getWidth() / 2 + 122, y);
        g.drawString("Prix", getWidth() / 2 + 222, y);
        g.drawString("Prix Achat", getWidth() / 2 + 322, y);
        g.drawString("Value", getWidth() / 2 + 422, y);
        g.setFont(new Font("default", Font.ROMAN_BASELINE, 14));
        y += 15;
        Iterator<String> itPTF = Utilisateur.INSTANCE.getPF().keySet().iterator();
        while (itPTF.hasNext()) {
            ActionPf a = Utilisateur.INSTANCE.getPF().get(itPTF.next());
            ButtonSell bt = new ButtonSell(a);
            add(bt);
            bt.setBounds(getWidth()-60, y - 14, 50, 14);
            bt.setBorder(BorderFactory.createEmptyBorder());
            g.drawString(a.getName(), getWidth() / 2 + 2, y);
            g.drawString(Integer.toString(a.getQuantite()), getWidth() / 2 + 122, y);
            g.drawString(Double.toString(a.getPrixActuel()) + "€", getWidth() / 2 + 222, y);
            g.drawString(Double.toString(a.getBuyPrice()) + "€", getWidth() / 2 + 322, y);
            colorString(g, a.getPlusValue(), getWidth() / 2 + 422, y);
            y += 15;
        }
        /*Affichage de la liste d'attente d'achat*/
        g.setFont(new Font("default", Font.BOLD, 14));
        g.drawString("Ordres d'achat en attentes :", getWidth() / 2 + 2, y);
        y += 15;
        g.drawString("Société", getWidth() / 2 + 2, y);
        g.drawString("Quantité", getWidth() / 2 + 122, y);
        g.drawString("Prix Max", getWidth() / 2 + 222, y);
        g.drawString("ExpirationDate", getWidth() / 2 + 322, y);
        g.setFont(new Font("default", Font.ROMAN_BASELINE, 14));
        y+=15;
        Iterator<String> itBuy = Utilisateur.INSTANCE.getBuyAttentes().keySet().iterator();
        while (itBuy.hasNext()) {
            ActionAchatPrecis a = Utilisateur.INSTANCE.getBuyAttentes().get(itBuy.next());
            g.drawString(a.getName(), getWidth() / 2 + 2, y);
            g.drawString(Integer.toString(a.getQuantite()), getWidth() / 2 + 122, y);
            g.drawString(Double.toString(a.getPrixMax()) + "€", getWidth() / 2 + 222, y);
            Date d = new Date(a.getExpirationDate());
            g.drawString(d.toString() , getWidth() / 2 + 322, y);
            y += 15;
        }
        /*Affichage de la liste d'attente de vente*/
        g.setFont(new Font("default", Font.BOLD, 14));
        g.drawString("Ordres de vente en attentes :", getWidth() / 2 + 2, y);
        y += 15;
        g.drawString("Société", getWidth() / 2 + 2, y);
        g.drawString("Quantité", getWidth() / 2 + 122, y);
        g.drawString("Prix Min", getWidth() / 2 + 222, y);
        g.drawString("ExpirationDate", getWidth() / 2 + 322, y);
        g.setFont(new Font("default", Font.ROMAN_BASELINE, 14));
        y+=15;
        Iterator<String> itSell = Utilisateur.INSTANCE.getSellAttentes().keySet().iterator();
        while (itSell.hasNext()) {
            ActionVentePrecis a = Utilisateur.INSTANCE.getSellAttentes().get(itSell.next());
            g.drawString(a.getName(), getWidth() / 2 + 2, y);
            g.drawString(Integer.toString(a.getQuantite()), getWidth() / 2 + 122, y);
            g.drawString(Double.toString(a.getPrixMin()) + "€", getWidth() / 2 + 222, y);
            Date d = new Date(a.getExpirationDate());
            g.drawString(d.toString() , getWidth() / 2 + 322, y);
            y += 15;
        }
        g.setFont(new Font("default", Font.BOLD, 14));
        g.drawString("Valeur du PTF: " + Utilisateur.INSTANCE.getValPTF(), getWidth() / 2 + 2, getHeight() - 17);
        colorString(g, Utilisateur.INSTANCE.getValuePTF(), getWidth() / 2 + 2, getHeight() - 2);
    }

    public void colorString(Graphics g, double val, int x, int y) {
        if (val < 0) {
            g.setColor(Color.RED);
        } else {
            g.setColor(Color.GREEN);
        }
        g.drawString(Double.toString(val) + "%", x, y);
        g.setColor(Color.BLACK);
    }

    @Override
    public void miseAJour() {
        repaint();
    }
}
