package gui;

import controleur.Controleur;
import java.awt.event.*;

public class SellDialog extends ActionDialog {

    public SellDialog(int x, int y, modele.Action a) {
        super(x, y, a, new SellPanel(a));
        setTitle("SELL " + a.getName());
        /*On vend l'action à la fermeture d ela fenetre */
        getBtOk().addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    int quantite = Integer.parseInt(getQtyField());
                    double prix = a.getPrixActuel();
                    if (quantite != 0) {
                        if (!getPriceField().isEmpty()) {
                            prix = Double.parseDouble(getPriceField());
                        }
                        if (prix == a.getPrixActuel()) {
                            /*vente direct*/
                            Controleur.sell(a.getName(), quantite, a.getPrixActuel());
                        } else {
                            Controleur.addSellAttente(a.getName(), quantite, prix);
                            /*vente en attente*/
                        }
                    }
                } catch (NumberFormatException ex) {

                }
            }
        });

    }

}
