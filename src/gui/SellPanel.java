package gui;

import java.awt.Graphics;
import javax.swing.*;

public class SellPanel extends ActionPanel {

    public SellPanel(modele.Action a) {
        super(a);
    }

    @Override
    protected void paintComponent(Graphics g) {
        g.clearRect(0, 0, getWidth(), getHeight());
        g.drawString("Vente de l'action " + getActif().getName() + " au prix de " + getActif().getPrixActuel() + "€/µ", 5, 20);
        g.drawString("Qty : ", 20, 45);
        g.drawString("prix Précis : ", 150, 45);
        g.drawString("(pressez 'entrée' pour obtenir le cout)", 20, 70);
        g.drawString("Cout de l'opération +" + getCout() + "€", 20, 95);
        g.drawString("Vente si le cours de l'actif atteint " + getPrixAttendu()+"€", 20, 105);
    }
    
    
}
