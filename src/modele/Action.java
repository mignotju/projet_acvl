package modele;

public class Action {

    //Nom de l'action
    private String name;
    protected double prixActuel;

    public Action(String n) {
        name = n;
        prixActuel = 0;
    }
    
    public Action(String n, double Prix) {
        name = n;
        prixActuel = Prix;
    }

    public String getName() {
        return name;
    }

    public double getPrixActuel() {
        return prixActuel;
    }
    
    public void setPrice(double p){
        prixActuel = p;
    }

}
