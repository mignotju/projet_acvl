package modele;

public class ActionAchatPrecis extends ActionAttentes {

    private double prixMax; //d'achat, prix maximum auquel on peut acheter. si prix actuel > prix max, on n'achete pas

    public ActionAchatPrecis(long date, String name, int quantite, double priceMax) {
        super(date, name, quantite);
        prixMax = priceMax;
    }
    
    public ActionAchatPrecis(long date, String name, int quantite, double priceMax, double prixActuel) {
        super(date, name, quantite, prixActuel);
        prixMax = priceMax;
    }
    
    public double getPrixMax(){
        return prixMax;
    }
}
