package modele;

public class ActionAttentes extends Action {
    private int quantite;
    private long dateEmissionOrdre;
    private long expirationDate; //la vente sera annulé dans tant de temps si l'ordre n'est pas passé
    private final long tempsAttenteMax = 172800 * 1000; // 2 jours en secondes, plus réaliste
//    private final long tempsAttenteMax = 10 * 1000; // 10 secondes, utiles pour tests
    
    public ActionAttentes(long date, String name, int quantite) {
        super(name);
        dateEmissionOrdre = date;
        expirationDate = date + tempsAttenteMax;
        this.quantite = quantite;
    }
    
    
    public ActionAttentes(long date, String name, int quantite, double prixActuel) {
        super(name, prixActuel);
        dateEmissionOrdre = date;
        expirationDate = date + tempsAttenteMax;
        this.quantite = quantite;
    }
    
    public int getQuantite(){
        return quantite;
    }
    
    public long getDateEmissionOrdre(){
        return dateEmissionOrdre;
    }
    
    public long getExpirationDate() {
        return expirationDate;
    }
}   
