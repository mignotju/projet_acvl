package modele;

public class ActionCours extends Action {

    private double prixOpen = 1;
    private double valueOpen = 0;/*Plus ou moins value par rapport à l'ouverture*/


    public ActionCours(String n, double prix, double open) {
        super(n, prix);
        prixOpen = open;
        majValueOpen();
    }

    public double getPrixOpen() {
        return prixOpen;
    }

    public void setPrixOpen(double p) {
        prixOpen = p;
        majValueOpen();
    }

    public double getValueOpen() {
        return valueOpen;
    }
    
    private void majValueOpen(){
        valueOpen =100 * (prixActuel - prixOpen) / prixOpen;
        valueOpen = Math.round(100*valueOpen)/100.0;
    }

}
