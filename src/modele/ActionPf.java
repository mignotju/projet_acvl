package modele;
import modele.Action;

public class ActionPf extends Action {

    private int quantite;
    private double buyPrice;
    private double plusValue;

    public ActionPf(String name, int quantite, double price) {
        super(name, price);
        this.quantite = quantite;
        buyPrice = price;
        majPlusValue();
    }

    public ActionPf(String name, int quantite, double price, double prixActuel) {
        super(name,prixActuel);
        this.quantite = quantite;
        buyPrice = price;
        majPlusValue();
    }

    public int getQuantite() {
        return quantite;
    }

    public double getBuyPrice() {
        return buyPrice;
    }

    public double getPlusValue() {
        return plusValue;
    }

    private void majPlusValue() {
        plusValue = 100 * (prixActuel - buyPrice) / buyPrice;
        plusValue = Math.round(100 * plusValue) / 100.0;
    }

    @Override
    public void setPrice(double p) {
        prixActuel = p;
        majPlusValue();
    }
}
