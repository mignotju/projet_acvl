package modele;

public class ActionVentePrecis extends ActionAttentes{
    private double prixMin; //de vente, prix minimum auquel on peut vendre. si prix actuel < prix min, on ne vend pas

    public ActionVentePrecis(long date, String name, int quantite, double priceMin) {
        super(date, name, quantite);
        prixMin = priceMin;
    }
    
    public ActionVentePrecis(long date, String name, int quantite, double priceMin, double prixActuel) {
        super(date, name, quantite,prixActuel);
        prixMin = priceMin;
    }
    
    public double getPrixMin(){
        return prixMin;
    }
    
    
}
