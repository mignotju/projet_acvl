package modele;

import java.io.*;
import java.net.URL;
import java.util.*;

public class Cours extends Sujet {

    private Map<String, ActionCours> liste;

    public Cours() {
        liste = new HashMap<String, ActionCours>();
    }


    public void miseAJourBourse() {
        //Utile pour créer l'action à ajouter.
        String nomAction;
        double prixOuverture;
        double prixActuel = 0;

        //Récupere le code source d'une page en ligne
        String url = "http://www.abcbourse.com/marches/indice_cac40.aspx";
        //String dans laquelle je récupere le code source de la page ci dessus
        String code;
        code = "";
        BufferedReader in = null;
        try {
            URL site = new URL(url);
            in = new BufferedReader(new InputStreamReader(site.openStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                code = code + "\n" + (inputLine);
            }
            in.close();
        } catch (IOException ex) {
        } finally {
            try {
                in.close();
            } catch (IOException ex) {
            }
        }

        //Je mets tout dans le fichier cours.txt
        try {
            FileWriter fw = new FileWriter("cours.txt", false);
            fw.write(code);
            fw.flush();
            fw.close();
        } catch (IOException e) {
            System.exit(0);
        }

        //lecture du fichier texte	
        int indexOf; //pour des sous-chaines
        String sousLigne;
        try {
            InputStream ips = new FileInputStream("cours.txt");
            InputStreamReader ipsr = new InputStreamReader(ips);
            BufferedReader br = new BufferedReader(ipsr);
            String ligne;
            //tant que je peux récupérer des lignes
            while ((ligne = br.readLine()) != null) {
                //Les 40 blocs d'actions commencent par cette ligne
                if (ligne.contains("/graphes/display.aspx?s=")) {
                    //une seule autre ligne contient cette sousString, elle contient aussi cac40
                    //on la vire
                    if (ligne.contains("Cac 40")) {
                        continue;
                    }
                    indexOf = ligne.indexOf("/graphes/display.aspx?s=");
                    sousLigne = ligne.substring(indexOf + 29);
                    indexOf = sousLigne.indexOf("<");
                    sousLigne = sousLigne.substring(0, indexOf);
                    //Ici j'ai dans la sous ligne le nom.
                    //Je peux avoir peut etre ce caractere en plus, si il y est je l'enelve.
                    indexOf = sousLigne.indexOf(">");
                    if (indexOf != -1) {
                        sousLigne = sousLigne.substring(indexOf + 1);
                    }
                    //utile pour créer l'actionCour après
                    nomAction = sousLigne;
                    ligne = br.readLine();
                    indexOf = ligne.indexOf(">");
                    sousLigne = ligne.substring(indexOf + 1);
                    indexOf = sousLigne.indexOf("<");
                    sousLigne = sousLigne.substring(0, indexOf);
                    //utile pour créer l'actionCour après
                    prixOuverture = Double.parseDouble(sousLigne);
                    //On a 6 lignes inutiles a sauter.
                    for (int i = 1; i < 6; i++) {
                        ligne = br.readLine();
                    }
                    indexOf = ligne.indexOf(">");
                    if (indexOf == 15) {
                        sousLigne = ligne.substring(indexOf + 4);
                        indexOf = sousLigne.indexOf("<");
                        sousLigne = sousLigne.substring(0, indexOf);
                        //utile pour créer l'actionCour après
                        prixActuel = Double.parseDouble(sousLigne);
                    }
                    //Je crée l'action avec les bonnes caracts, et la met dans la liste.
                    ActionCours action = new ActionCours(nomAction, prixActuel, prixOuverture);
                    liste.put(nomAction, action);
                }
            }
            br.close();
            informe();
            Utilisateur.INSTANCE.misaAJourPTF(this);
        } catch (Exception e) {
        }
    }

    public Map<String, ActionCours> getListe() {
        return liste;
    }

    public Iterator<String> getIterator() {
        return getListe().keySet().iterator();
    }
}
