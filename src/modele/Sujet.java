package modele;

import java.util.Set;
import gui.*;
import java.util.HashSet;

public abstract class Sujet {
    private Set<Observateur> obs;
    
    public Sujet() {
        obs = new HashSet<Observateur>();
    }
    
    public void attache(Observateur o) {
        obs.add(o);
    }
    
    public void detache(Observateur o) {
        obs.remove(o);
    }
    
    public void informe() {
        for(Observateur o: obs) {
            o.miseAJour();
        }
    }
    
    
}
