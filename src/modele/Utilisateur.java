package modele;

import controleur.Controleur;
import modele.Action;
import java.util.*;

public class Utilisateur extends Sujet {

    public static Utilisateur INSTANCE = new Utilisateur(20000);
    private Map<String, ActionPf> pf;
    private Map<String, ActionAchatPrecis> myBuyAttentes;
    private Map<String, ActionVentePrecis> mySellAttentes;
    private double cash;
    private final double cashInitial;

    private Utilisateur(double cashIn) {
        cash = cashIn;
        cashInitial = cashIn;
        pf = new HashMap<String, ActionPf>();
        myBuyAttentes = new HashMap<String, ActionAchatPrecis>();
        mySellAttentes = new HashMap<String, ActionVentePrecis>();
    }

    private Utilisateur() {
        cash = 10000;
        cashInitial = 10000;
        pf = new HashMap<String, ActionPf>();
        myBuyAttentes = new HashMap<String, ActionAchatPrecis>();
        mySellAttentes = new HashMap<String, ActionVentePrecis>();
    }

    public void misaAJourPTF(Cours c) {
        for (String name : pf.keySet()) {
            pf.get(name).setPrice(c.getListe().get(name).getPrixActuel());
        }
        informe();
    }

    public double getCash() {
        return cash;
    }

    public void setCash(double p) {
        cash = p;
        informe();
    }

    public double getCashInit() {
        return cashInitial;
    }

    public boolean possedeAction(Action a) {
        return false;
    }

    public Map<String, ActionPf> getPF() {
        return pf;
    }

    public Map<String, ActionAchatPrecis> getBuyAttentes() {
        return myBuyAttentes;
    }

    public Map<String, ActionVentePrecis> getSellAttentes() {
        return mySellAttentes;
    }

    public double getValPTF() {
        double res = cash;
        for (String name : pf.keySet()) {
            res += pf.get(name).getPrixActuel() * pf.get(name).getQuantite();
        }
        return res;
    }

    public double getValuePTF() {
        double res = 100 * (getValPTF() - cashInitial) / cashInitial;
        res = Math.round(res * 10000) / 10000.0;
        return res;
    }

    public void miseAJourAttentes(Cours c) {
        ActionAchatPrecis ActionToBuy;
        Date d = new Date();
        //Je parcous ma liste d'achat à un prix fixé
        for (String name : myBuyAttentes.keySet()) {
            ActionToBuy = myBuyAttentes.get(name);
            if (c.getListe().get(name).getPrixActuel() <= ActionToBuy.getPrixMax()) {
                //J'achete
                Controleur.buy(name, ActionToBuy.getQuantite(), c.getListe().get(name).getPrixActuel());
                //j'enleve cet ordre de la liste des achats.
                myBuyAttentes.remove(name);
            } else if (ActionToBuy.getExpirationDate() <= d.getTime()) {
                //Le temps d'expiration est depassé.
                //J'enelve l'ordre
                myBuyAttentes.remove(name);
            }
        }
        
        //Pareil pour les ordres de vente à un prix fixé.
        ActionVentePrecis ActionToSell;
        for (String name : mySellAttentes.keySet()) {
            //Si le prix du cour est plus haut que le prix min de vente
            ActionToSell = mySellAttentes.get(name);
            if (c.getListe().get(name).getPrixActuel() >= ActionToSell.getPrixMin()) {
                //Je vend, et j'enleve de la liste
                Controleur.sell(name, ActionToSell.getQuantite(), c.getListe().get(name).getPrixActuel());
                mySellAttentes.remove(name);
            } else if (ActionToSell.getExpirationDate() <= d.getTime()) {
                //Le temps d'attente est dépassé, j'enleve l'odre de la liste
                mySellAttentes.remove(name);
            }
        }
        informe();
    }
}
